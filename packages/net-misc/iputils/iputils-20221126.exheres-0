# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

require github meson

SUMMARY="Small useful utilities for networking"
HOMEPAGE+=" https://wiki.linuxfoundation.org/networking/${PN}"
LICENCES="BSD-4 || ( GPL-2 GPL-3 ) as-is"

SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    caps
    idn
"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        app-text/docbook-xsl-ns-stylesheets
        dev-libs/libxslt [[ note = [ xsltproc ] ]]
        sys-devel/gettext
    build+run:
        caps? ( sys-libs/libcap )
        idn? ( net-dns/libidn2:= )
    test:
        sys-apps/iproute2 [[ note = ip ]]
    suggestion:
        net-analyzer/traceroute [[
            description = [ Traceroute is a tool commonly categorized with iputils ]
        ]]
"

# Tests need a lot of whitelisting including @0 and one IPv6 test still
# doesn't work.
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    -DBUILD_ARPING=true
    -DUSE_GETTEXT=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'caps USE_CAP'
    'idn USE_IDN'
)
MESON_SRC_CONFIGURE_TESTS=( '-DSKIP_TESTS=false -DSKIP_TESTS=true' )

src_test() {
    esandbox allow_net --connect "inet:127.0.0.1@1025"
    esandbox allow_net --connect "inet:127.0.0.1@0"
    esandbox allow_net --connect "inet6:::1@58"
    esandbox allow_net --connect "inet6:::1@1025"

    meson_src_test

    esandbox disallow_net --connect "inet6:::1@1025"
    esandbox disallow_net --connect "inet6:::1@58"
    esandbox disallow_net --connect "inet:127.0.0.1@0"
    esandbox disallow_net --connect "inet:127.0.0.1@1025"
}

pkg_postinst() {
    if option caps; then
        nonfatal edo setcap cap_net_raw+ep /usr/$(exhost --target)/bin/arping
        nonfatal edo setcap cap_net_raw+ep /usr/$(exhost --target)/bin/clockdiff
        nonfatal edo setcap cap_net_raw+ep /usr/$(exhost --target)/bin/ping
    fi
}

