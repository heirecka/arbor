# Copyright 2009 Bryan Østergaard
# Copyright 2012-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service
require python [ blacklist='2' multibuild=false with_opt=true ]
require bash-completion

export_exlib_phases src_configure src_test src_install

SUMMARY="Support utilities for handling btrfs filesystems"
HOMEPAGE="https://btrfs.readthedocs.io/"

UPSTREAM_CHANGELOG="${HOMEPAGE}latest/CHANGES.html"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    zoned [[ description = [ Support for Zoned Storage Devices ] ]]

    providers: libgcrypt [[ description = [ Prefer libgcrypt over the build-in hash implementation ] ]]
    providers: libsodium [[ description = [ Prefer libsodium over the built-in hash implementation ] ]]

    ( providers: libgcrypt libsodium ) [[ number-selected = at-most-one ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-python/Sphinx
        virtual/pkg-config[>=0.9.0]
        python? ( dev-python/setuptools:*[python_abis:*(-)?] )
    build+run:
        app-arch/lzo:2 [[ note = [ could be optional, but not recommended ] ]]
        app-arch/zstd[>=1.0.0]
        sys-apps/acl
        sys-fs/e2fsprogs[>=1.42]
        providers:eudev? ( sys-apps/eudev )
        providers:libgcrypt? ( dev-libs/libgcrypt[>=1.8.0] )
        providers:libsodium? ( dev-libs/libsodium[>=1.0.4] )
        providers:systemd? ( sys-apps/systemd )
        zoned? ( sys-kernel/linux-headers[>=5.9] )
    test:
        sys-fs/lvm2 [[ note = dmsetup ]]
        sys-libs/liburing
    suggestion:
        dev-python/matplotlib [[ description = [ for btrfs-show-blocks & other debugging tools ] ]]
"

DEFAULT_SRC_COMPILE_PARAMS=(
    V=1
)

DEFAULT_SRC_INSTALL_PARAMS=(
    mandir=/usr/share/man
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=(
    INSTALL
    show-blocks
)

AT_M4DIR=( m4 )

btrfs-progs_src_configure() {
    local myconf=(
        --enable-libudev
        --enable-zstd
        --disable-experimental
        --disable-static
        # There's also support to convert reiserfs, but it needs
        # reiserfsprogs[>=3.6.27] which we don't have yet.
        --with-convert=ext2
        $(option_enable python)
        $(option_enable zoned)
    )

    if [[ $(exhost --target) == *-musl ]];then
        myconf+=( --disable-backtrace )
    fi

    if $(option providers:libgcrypt) ; then
        myconf+=( --with-crypto=libgcrypt )
    elif $(option providers:libsodium) ;then
        myconf+=( --with-crypto=libsodium )
    else
        myconf+=( --with-crypto=builtin )
    fi

    econf "${myconf[@]}"
}

btrfs-progs_src_test() {
    # prevent trying to run modprobe
    edo sed \
        -e 's:run_check $SUDO_HELPER modprobe btrfs::g' \
        -i tests/common

    edo mkdir "${TEMP}"/{dev,mnt}
    edo sed \
        -e "/TEST_DEV=/s:$:${TEMP}/dev:" \
        -e "/TEST_MNT=/s:$:${TEMP}/mnt:" \
        -i tests/fsck-tests.sh

    emake -j1 test
}

btrfs-progs_src_install() {
    if option python ; then
        emake -j1 DESTDIR="${IMAGE}" "${DEFAULT_SRC_INSTALL_PARAMS[@]}" install install_python
        emagicdocs
    else
        default
    fi

    install_systemd_files
    # For potentially critical filesystem services (e. g. btrfs, lvm2) we auto-
    # activate the respective service.
    dodir "${SYSTEMDSYSTEMUNITDIR}"/basic.target.wants
    dosym ../btrfs.service "${SYSTEMDSYSTEMUNITDIR}"/basic.target.wants/btrfs.service

    dobashcompletion "${WORK}"/btrfs-completion btrfs
}

