Upstream: yes

From 5cf37adb380b4ddf819951913c4d3044071f7e6b Mon Sep 17 00:00:00 2001
From: Victor Westerhuis <victor@westerhu.is>
Date: Fri, 3 Dec 2021 14:41:24 +0100
Subject: [PATCH] Make tests buildable in out-of-tree build

Let libtool take care of the linking. Prefer linking to libcolm
statically, like before. Add explicit include paths for files included
from the corresponding source directory.
---
 configure.ac               |  3 ++-
 test/aapl.d/Makefile.am    | 10 +++++++++-
 test/colm.d/Makefile.am    |  9 ++++++++-
 test/colm.d/gentests.sh    |  2 +-
 test/rlhc.d/Makefile.am    | 14 ++++++++++++--
 test/rlparse.d/Makefile.am | 21 ++++++++++++++++-----
 test/trans.d/Makefile.am   | 20 +++++++++++++++-----
 test/trans.d/gentests      |  2 +-
 8 files changed, 64 insertions(+), 17 deletions(-)

diff --git a/configure.ac b/configure.ac
index 230805b4..9248bc63 100644
--- a/configure.ac
+++ b/configure.ac
@@ -46,6 +46,7 @@ AC_PROG_CXX
 AC_CHECK_TOOL(AR, ar)
 AC_PROG_RANLIB
 AC_PROG_LIBTOOL
+AC_PROG_LN_S
 
 SED_SUBST=["\
 	-e 's|@CXX@|${CXX}|g' \
@@ -186,7 +187,7 @@ AC_ARG_WITH(subject,
 		SUBJ_RAGEL_JULIA_BIN="$withval/bin/ragel-julia"
 	],
 	[
-		SUBJ_AAPL_CPPFLAGS='-I$(abs_top_builddir)/src/aapl'
+		SUBJ_AAPL_CPPFLAGS='-I$(abs_top_srcdir)/src/aapl'
 
 		SUBJ_COLM_BIN='$(abs_top_builddir)/src/colm'
 		SUBJ_COLM_CPPFLAGS='-I$(abs_top_builddir)/src/include'
diff --git a/test/aapl.d/Makefile.am b/test/aapl.d/Makefile.am
index 906b94ef..4aade8ff 100644
--- a/test/aapl.d/Makefile.am
+++ b/test/aapl.d/Makefile.am
@@ -102,4 +102,12 @@ gentests: gentests.sh Makefile
 #-include $(DEPS)
 
 clean-local:
-	rm -Rf working
+	rm -Rf working *.exp
+
+all-local:
+	if test '$(builddir)' != '$(srcdir)'; then \
+		$(LN_S) -f '$(srcdir)/'*.exp '$(builddir)'; \
+	fi
+
+check-local:
+	'$(top_srcdir)/test/runtests'
diff --git a/test/colm.d/Makefile.am b/test/colm.d/Makefile.am
index e2a4c911..a728facb 100644
--- a/test/colm.d/Makefile.am
+++ b/test/colm.d/Makefile.am
@@ -186,5 +186,12 @@ gentests: gentests.sh Makefile
 	@$(top_srcdir)/sedsubst $< $@ -w,+x $(SED_SUBST)
 
 clean-local:
-	rm -Rf working
+	rm -Rf working *.lm *.lmi *.in
 
+all-local:
+	if test '$(builddir)' != '$(srcdir)'; then \
+		$(LN_S) -f '$(srcdir)/'*.lm '$(srcdir)/'*.lmi '$(srcdir)/'*.in '$(builddir)'; \
+	fi
+
+check-local:
+	'$(top_srcdir)/test/runtests'
diff --git a/test/colm.d/gentests.sh b/test/colm.d/gentests.sh
index b5cc8982..783f002f 100644
--- a/test/colm.d/gentests.sh
+++ b/test/colm.d/gentests.sh
@@ -226,7 +226,7 @@ function runtests()
 			fi
 		else
 			# Compilation.
-			echo $COLM_BIN $COMP $COLM_ADDS $LM '&>' $LOG >> $SH
+			echo $COLM_BIN -B ../.. $COMP $COLM_ADDS $LM '&>' $LOG >> $SH
 			echo "if [ \$? != 0 ]; then echo \"COMPILATION FAILED (see $LOG)\" >> $DIFF; fi" >> $SH
 		fi
 
diff --git a/test/rlhc.d/Makefile.am b/test/rlhc.d/Makefile.am
index 4e9ffe56..4c8e841d 100644
--- a/test/rlhc.d/Makefile.am
+++ b/test/rlhc.d/Makefile.am
@@ -19,7 +19,7 @@ EXTRA_DIST = gentests rlhc.lm $(RLHC_DEP) $(CASES)
 noinst_PROGRAMS = rlhc
 
 rlhc$(EXEEXT): rlhc.lm $(RLHC_DEP) $(COLM_BIN)
-	$(COLM_BIN) -b rlhc_object -o $@ $<
+	$(COLM_BIN) -B '$(top_builddir)' -b rlhc_object -o $@ -I$(srcdir) $<
 
 CASES = \
 	case/atoi1_c.c-C-F1--goto-backend.exp \
@@ -1089,5 +1089,15 @@ CASES = \
 	case/union.cpp-C-T1--goto-backend.exp \
 	case/union.cpp-C-T1--goto-backend.in
 
+CLEANFILES = gentests
+
 clean-local:
-	rm -Rf working
+	rm -Rf working case
+
+all-local:
+	if test '$(builddir)' != '$(srcdir)'; then \
+		$(LN_S) -f '$(srcdir)/gentests' '$(srcdir)/case' '$(builddir)'; \
+	fi
+
+check-local:
+	'$(top_srcdir)/test/runtests'
diff --git a/test/rlparse.d/Makefile.am b/test/rlparse.d/Makefile.am
index 54a14639..479332ef 100644
--- a/test/rlparse.d/Makefile.am
+++ b/test/rlparse.d/Makefile.am
@@ -1,6 +1,6 @@
 COLM_BIN = ../../src/colm
 COLM_xCPPFLAGS = -I../../src/include
-COLM_xLDFLAGS = -L../../src
+COLM_xLDADD = ../../src/libcolm.la
 
 noinst_PROGRAMS = rlparse
 
@@ -21,8 +21,8 @@ rlparse_SOURCES = parse.c if.h if.cc commit.cc \
 	svector.h
 
 rlparse_CPPFLAGS = $(COLM_xCPPFLAGS)
-rlparse_LDADD = -lcolm
-rlparse_LDFLAGS = $(COLM_xLDFLAGS)
+rlparse_LDADD = $(COLM_xLDADD)
+rlparse_LDFLAGS = -static
 
 EXTRA_DIST = \
 	reducer.lm rlparse.lm ragel.lm \
@@ -34,11 +34,22 @@ EXTRA_DIST = \
 BUILT_SOURCES = parse.c if.h if.cc commit.cc
 
 parse.c: rlparse.lm reducer.lm $(RAGEL_LM) $(COLM_BIN)
-	$(COLM_BIN) -c -b rlparse_object -o $@ -e if.h -x if.cc -m commit.cc $<
+	$(COLM_BIN) -c -b rlparse_object -o $@ -e if.h -x if.cc -m commit.cc -I$(srcdir) $<
 
 if.h: parse.c
 if.cc: parse.c
 commit.cc: parse.c
 
+CLEANFILES = gentests
+
 clean-local:
-	rm -Rf working
+	rm -Rf working case
+
+all-local:
+	if test '$(builddir)' != '$(srcdir)'; then \
+		$(LN_S) -f '$(srcdir)/gentests' '$(builddir)'; \
+		cp -pR -f '$(srcdir)/case' '$(builddir)'; \
+	fi
+
+check-local:
+	'$(top_srcdir)/test/runtests'
diff --git a/test/trans.d/Makefile.am b/test/trans.d/Makefile.am
index 4a9d8798..30b53ff1 100644
--- a/test/trans.d/Makefile.am
+++ b/test/trans.d/Makefile.am
@@ -1,7 +1,7 @@
 COLM_BIN = ../../src/colm
 
 COLM_xCPPFLAGS = -I../../src/include
-COLM_xLDFLAGS = -L../../src
+COLM_xLDADD = ../../src/libcolm.la
 
 noinst_PROGRAMS = trans
 
@@ -29,12 +29,12 @@ EXTRA_DIST = \
 	$(CASES)
 
 trans.c: trans.lm $(TRANS_DEPS) $(COLM_BIN)
-	$(COLM_BIN) -b trans_object -c -o $@ $<
+	$(COLM_BIN) -b trans_object -c -o $@ -I$(srcdir) $<
 
 trans_CPPFLAGS = $(COLM_xCPPFLAGS)
 trans_SOURCES = actparams.cc trans.c main.c
-trans_LDADD = -lcolm
-trans_LDFLAGS = $(COLM_xLDFLAGS)
+trans_LDADD = $(COLM_xLDADD)
+trans_LDFLAGS = -static
 
 CASES =  \
 	case/any1_asm.rl \
@@ -378,5 +378,15 @@ CASES =  \
 	case/zlen1_ruby.rl \
 	case/zlen1_rust.rl
 
+CLEANFILES = gentests
+
 clean-local:
-	rm -Rf working
+	rm -Rf working case
+
+all-local:
+	if test '$(builddir)' != '$(srcdir)'; then \
+		$(LN_S) -f '$(srcdir)/gentests' '$(srcdir)/case' '$(builddir)'; \
+	fi
+
+check-local:
+	'$(top_srcdir)/test/runtests'
diff --git a/test/trans.d/gentests b/test/trans.d/gentests
index a2064080..dd3bcf91 100755
--- a/test/trans.d/gentests
+++ b/test/trans.d/gentests
@@ -9,7 +9,7 @@ mkdir -p working
 
 echo working/* | xargs rm -f
 
-[ $# = 0 ] && set -- `find case -type f -and -not -name '*_*' | sort`
+[ $# = 0 ] && set -- `find -H case -type f -and -not -name '*_*' | sort`
 
 for fn; do 
 	
-- 
2.35.1

