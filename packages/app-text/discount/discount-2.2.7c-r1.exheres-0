# Copyright 2015-2016 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Orc tag=v${PV} ]
require alternatives

SUMMARY="An implementation in C of John Gruber's Markdown"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    cxx [[ description = [ Build C++ bindings ] ]]
    debug
"

DEPENDENCIES="
    run:
        !app-text/discount:0[<2.2.7c-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

# Fix -lmarkdown linking error by following make build order
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

src_prepare() {
    default

    edo sed \
        -e '/test "\$LDCONFIG"/d' \
        -e 's/_strip="-s"/unset _strip/' \
        -i configure.inc
}

src_configure() {
    local myconf=(
        --prefix=/usr/$(exhost --target)
        --mandir=/usr/share/man
        --pkg-config
        --shared
        --enable-all-features
        --with-latex
        $(optionq cxx && echo --cxx-binding)
        $(optionq debug && echo --enable-amalloc)
    )

    edo ./configure.sh "${myconf[@]}"
}

src_install() {
    local arch_dependent_alternatives=() other_alternatives=()
    local host=$(exhost --target)

    emake install.everything DESTDIR="${IMAGE}"

    local binaries=( makepage markdown mkd2html theme )
    for binary in ${binaries[@]} ; do
        arch_dependent_alternatives+=(
            /usr/$(exhost --target)/bin/${binary} ${binary}-${SLOT}
        )
        other_alternatives+=(
            /usr/share/man/man1/${binary}.1 ${binary}-${SLOT}.1
        )
    done

    arch_dependent_alternatives+=(
        /usr/$(exhost --target)/include/mkdio.h     mkdio-${SLOT}.h
        /usr/$(exhost --target)/lib/libmarkdown.so  libmarkdown-${SLOT}.so
        /usr/$(exhost --target)/lib/pkgconfig/libmarkdown.pc libmarkdown-${SLOT}.pc
    )

    local man_pages=( markdown.3 mkd-callbacks.3 mkd-functions.3 mkd-line.3
        mkd_cleanup.3 mkd_compile.3 mkd_css.3 mkd_doc_author.3 mkd_doc_date.3
        mkd_doc_title.3 mkd_generatecss.3 mkd_generatehtml.3
        mkd_generateline.3 mkd_in.3 mkd_line.3 mkd_string.3
        markdown.7 mkd-extensions.7
    )
    for man in ${man_pages[@]} ; do
        other_alternatives+=(
            /usr/share/man/man${man#*.}/${man} ${man%.*}-${SLOT}.${man#*.}
        )
    done

    alternatives_for _$(exhost --target)_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${other_alternatives[@]}"
}

