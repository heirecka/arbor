# Copyright 2012-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=stachenov tag=v${PV} ] cmake
require alternatives

SUMMARY="Simple C++ wrapper over Gilles Vollant's ZIP/UNZIP package"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: qt5 qt6 ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build+run:
        sys-libs/zlib
        providers:qt5? ( x11-libs/qtbase:5 )
        providers:qt6? (
            x11-libs/qt5compat:6
            x11-libs/qtbase:6
        )
    run:
        !app-arch/quazip:0[<1.3-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

_quazip_conf() {
    local cmake_params=(
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DQUAZIP_ENABLE_TESTS:BOOL=$(expecting_tests TRUE FALSE)
        -DQUAZIP_INSTALL:BOOL=TRUE
        -DQUAZIP_USE_QT_ZLIB:BOOL=FALSE
        "$@"
    )

    ecmake "${cmake_params[@]}"
}

src_configure() {
    if option providers:qt5 ; then
        edo mkdir "${WORKBASE}"/qt5-build
        edo pushd "${WORKBASE}"/qt5-build
        _quazip_conf -DQUAZIP_QT_MAJOR_VERSION:STRING=5
        edo popd
    fi

    if option providers:qt6 ; then
        edo mkdir "${WORKBASE}"/qt6-build
        edo pushd "${WORKBASE}"/qt6-build
        _quazip_conf -DQUAZIP_QT_MAJOR_VERSION:STRING=6
        edo popd
    fi
}

src_compile() {
    if option providers:qt5 ; then
        edo pushd "${WORKBASE}"/qt5-build
        ecmake_build
        edo popd
    fi

    if option providers:qt6 ; then
        edo pushd "${WORKBASE}"/qt6-build
        ecmake_build
        edo popd
    fi
}

src_test() {
    if option providers:qt5 ; then
        edo pushd "${WORKBASE}"/qt5-build
        ectest
        edo popd
    fi

    if option providers:qt6 ; then
        edo pushd "${WORKBASE}"/qt6-build
        ectest
        edo popd
    fi
}

src_install() {
    local arch_dependent_alternatives=() host=$(exhost --target)

    if option providers:qt5 ; then
        edo pushd "${WORKBASE}"/qt5-build
        ecmake_install
        edo popd

        arch_dependent_alternatives+=(
            /usr/${host}/lib/libquazip1-qt5.so libquazip1-qt5-${SLOT}.so
            /usr/${host}/lib/pkgconfig/quazip1-qt5.pc quazip1-qt5-${SLOT}.pc
        )
    fi

    if option providers:qt6 ; then
        edo pushd "${WORKBASE}"/qt6-build
        ecmake_install
        edo popd

        arch_dependent_alternatives+=(
            /usr/${host}/lib/libquazip1-qt6.so libquazip1-qt6-${SLOT}.so
            /usr/${host}/lib/pkgconfig/quazip1-qt6.pc quazip1-qt6-${SLOT}.pc
        )
    fi

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"

    edo pushd "${CMAKE_SOURCE}"
    emagicdocs
    edo popd
}

