# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Copyright 2010 Sterling X. Winter <replica@exherbo.org>
# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require kernel.org [ repo=linux/kernel/git/stable/linux-stable ]
require kernel

export_exlib_phases pkg_pretend src_compile src_install

myexparam path=

DOWNLOADS="mirror://kernel/linux/kernel/v$(ever major).x/$(exparam path)/linux-${PV}.tar.xz"

SUMMARY="Linux kernel headers"
HOMEPAGE="https://www.kernel.org"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

RESTRICT="test"

DEPENDENCIES=""

WORK="${WORKBASE}/linux-${PV}"

_linux_headers_arch() {
    local host=$(exhost --target)

    case "${host}" in
    aarch64-*-linux-*)
        echo arm64
        ;;
    arm*-*-linux-*)
        echo arm
        ;;
    i686-*-linux-*)
        echo i386
        ;;
    ia64-*-linux-*)
        echo ia64
        ;;
    powerpc64-*-linux-*)
        echo powerpc
        ;;
    x86_64-*-linux-*)
        echo x86_64
        ;;
    *)
        die "Unsupported host '${host}', please update linux-headers.exlib"
        ;;
    esac
}

linux-headers_pkg_pretend() {
    if ! kernel_version_at_least ${PV} ; then
        ewarn "Kernel headers version mismatch: Installing kernel headers newer than the"
        ewarn "running kernel can break your system. Do not proceed unless you know what"
        ewarn "you're doing."
    fi
}

linux-headers_src_compile() {
    :
}

# TODO(?) need to sanitize headers at some point
linux-headers_src_install() {
    local target=$(exhost --target)

    edo mkdir -p "${IMAGE}/usr/${target}"
    edo emake                                     \
        ARCH=$(_linux_headers_arch)               \
        CROSS_COMPILE=$(exhost --tool-prefix)     \
        HOSTCC="${CHOST}-cc"                      \
        INSTALL_HDR_PATH="${IMAGE}/usr/${target}" \
        headers_install
    edo find "${IMAGE}/usr/${target}/include" -name '.install' -delete -or \
                                              -name '..install.cmd' -delete
}

