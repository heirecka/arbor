Upstream: under review, https://github.com/NixOS/patchelf/pull/493

From 7224237ca7c367414204d2759150f8b43728e8aa Mon Sep 17 00:00:00 2001
From: Heiko Becker <heirecka@exherbo.org>
Date: Mon, 24 Apr 2023 22:10:10 +0200
Subject: [PATCH] Also respect a prefixed nm and strings too

---
 configure.ac          |  2 ++
 tests/Makefile.am     |  2 +-
 tests/shared-rpath.sh | 17 +++++++++--------
 3 files changed, 12 insertions(+), 9 deletions(-)

diff --git a/configure.ac b/configure.ac
index 4129920..fee6302 100644
--- a/configure.ac
+++ b/configure.ac
@@ -7,9 +7,11 @@ AC_CONFIG_MACRO_DIR([m4])
 
 AC_CHECK_TOOL([STRIP], [strip])
 # Those are only used in tests, hence we gracefully degrate if they are not found.
+AC_CHECK_TOOL([NM], [nm], [nm])
 AC_CHECK_TOOL([OBJDUMP], [objdump], [objdump])
 AC_CHECK_TOOL([OBJCOPY], [objcopy], [objcopy])
 AC_CHECK_TOOL([READELF], [readelf], [readelf])
+AC_CHECK_TOOL([STRINGS], [strings], [strings])
 
 AM_PROG_CC_C_O
 AC_PROG_CXX
diff --git a/tests/Makefile.am b/tests/Makefile.am
index a68d2e2..8bbded7 100644
--- a/tests/Makefile.am
+++ b/tests/Makefile.am
@@ -61,7 +61,7 @@ TESTS = $(src_TESTS) $(build_TESTS)
 EXTRA_DIST = no-rpath-prebuild $(src_TESTS) no-rpath-prebuild.sh invalid-elf endianness empty-note \
 			 overlapping-segments-after-rounding short-first-segment.gz
 
-TESTS_ENVIRONMENT = PATCHELF_DEBUG=1 STRIP=$(STRIP) OBJDUMP=$(OBJDUMP) READELF=$(READELF) OBJCOPY=$(OBJCOPY)
+TESTS_ENVIRONMENT = PATCHELF_DEBUG=1 STRIP=$(STRIP) NM=$(NM) OBJDUMP=$(OBJDUMP) READELF=$(READELF) OBJCOPY=$(OBJCOPY) STRINGS=$(STRINGS)
 
 $(no_rpath_arch_TESTS): no-rpath-prebuild.sh
 	@ln -s $< $@
diff --git a/tests/shared-rpath.sh b/tests/shared-rpath.sh
index 8e6e26f..1fa46b9 100755
--- a/tests/shared-rpath.sh
+++ b/tests/shared-rpath.sh
@@ -2,7 +2,8 @@
 
 PATCHELF=$(readlink -f "../src/patchelf")
 SCRATCH="scratch/$(basename "$0" .sh)"
-READELF=${READELF:-readelf}
+NM=${NM:-nm}
+STRINGS=${STRINGS:-strings}
 
 LIB_NAME="${PWD}/libshared-rpath.so"
 
@@ -11,11 +12,11 @@ mkdir -p "${SCRATCH}"
 cd "${SCRATCH}"
 
 has_x() {
-    strings "$1" | grep -c "XXXXXXXX"
+    ${STRINGS} "$1" | grep -c "XXXXXXXX"
 }
 
-nm -D "${LIB_NAME}" | grep a_symbol_name
-previous_cnt="$(strings "${LIB_NAME}" | grep -c a_symbol_name)"
+${NM} -D "${LIB_NAME}" | grep a_symbol_name
+previous_cnt="$(${STRINGS} "${LIB_NAME}" | grep -c a_symbol_name)"
 
 echo "#### Number of a_symbol_name strings in the library: $previous_cnt"
 
@@ -25,12 +26,12 @@ echo "#### Rename the rpath to something larger than the original"
 "${PATCHELF}" --set-rpath a_very_big_rpath_that_is_larger_than_original  --output liblarge-rpath.so "${LIB_NAME}"
 
 echo "#### Checking symbol is still there"
-nm -D liblarge-rpath.so | grep a_symbol_name
+${NM} -D liblarge-rpath.so | grep a_symbol_name
 
 echo "#### Checking there are no Xs"
 [ "$(has_x liblarge-rpath.so)" -eq 0 ] || exit 1
 
-current_cnt="$(strings liblarge-rpath.so | grep -c a_symbol_name)"
+current_cnt="$(${STRINGS} liblarge-rpath.so | grep -c a_symbol_name)"
 echo "#### Number of a_symbol_name strings in the modified library: $current_cnt"
 [ "$current_cnt" -eq "$previous_cnt" ] || exit 1
 
@@ -40,10 +41,10 @@ echo "#### Rename the rpath to something shorter than the original"
 "${PATCHELF}" --set-rpath shrt_rpth  --output libshort-rpath.so "${LIB_NAME}"
 
 echo "#### Checking symbol is still there"
-nm -D libshort-rpath.so | grep a_symbol_name
+${NM} -D libshort-rpath.so | grep a_symbol_name
 
 echo "#### Number of a_symbol_name strings in the modified library: $current_cnt"
-current_cnt="$(strings libshort-rpath.so | grep -c a_symbol_name)"
+current_cnt="$(${STRINGS} libshort-rpath.so | grep -c a_symbol_name)"
 [ "$current_cnt" -eq "$previous_cnt" ] || exit 1
 
 echo "#### Now liblarge-rpath.so should have its own rpath, so it should be allowed to taint it"
-- 
2.40.0

