# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Small utility displaying info about file formats"
HOMEPAGE="http://www.darwinsys.com/${PN}"
DOWNLOADS="http://ftp.astron.com/pub/${PN}/${PNV}.tar.gz"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    zstd [[ description = [ Built-in zstd decompression support ] ]]
    parts: binaries data development documentation libraries
"

DEPENDENCIES="
    build+run:
        app-arch/bzip2
        app-arch/xz
        sys-libs/zlib
        zstd? ( app-arch/zstd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-bzlib
    --enable-xzlib
    --enable-zlib
    # doesn't work with sydbox
    --disable-libseccomp
    --disable-lzlib
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'zstd zstdlib'
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( MAINT PORTING )

src_install() {
    default

    edo rmdir "${IMAGE}"/usr/share/man/man5

    expart binaries /usr/$(exhost --target)/bin
    expart data /usr/share/misc
    expart development /usr/$(exhost --target)/include
    expart documentation /usr/share/{doc,man}
    expart libraries /usr/$(exhost --target)/lib
}

