# Copyright 2012-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# kmod has python bindings. Since we have fucked up our python crap so much, though,
# I can't get both multibuild C and python to work together.
# zlin did some work on this. Status unknown.
#NOTE(moben): on cross this should not pose a problem and we can re-enable the python bindings
require alternatives
require kernel.org [ repo=utils/kernel/kmod/kmod ]

export_exlib_phases src_install

SUMMARY="kmod - handle kernel modules"
DESCRIPTION="
kmod is a set of tools to handle common tasks with Linux kernel modules like
insert, remove, list, check properties, resolve dependencies, and aliases.
It replaces module-init-tools with which it is backwards-compatible.
"
DOWNLOADS="mirror://kernel/linux/utils/kernel/${PN}/${PNV}.tar.xz"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/plain/NEWS?h=v${PV}"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    bash-completion
    debug
    doc [[ description = [ Build libkmod's API documentation using gtk-doc ] ]]
    experimental [[ description = [ Enable experimental tools and features. Do not enable it unless you know what you are doing. ] ]]
    zstd
    ( providers: openssl [[
        description = [ Handle PKCS7 signatures via openssl>=1.1.0 ] ]]
    )
"

# testsuite/test-depmod fails with zstd disabled, last checked: 28
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        virtual/pkg-config
        doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        app-arch/xz
        sys-libs/zlib
        providers:openssl? ( dev-libs/openssl:=[>=1.1.0] )
        zstd? ( app-arch/zstd[>=1.4.4] )
        !sys-apps/module-init-tools [[
            description = [ module-init-tools has been replaced by kmod. ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-logging
    --enable-manpages
    --enable-tools
    --disable-coverage
    --disable-python
    --disable-test-modules
    --with-xz
    --with-zlib
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    "doc gtk-doc"
    experimental
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "bash-completion bashcompletiondir /usr/share/bash-completion/completions"
    "providers:openssl openssl"
    zstd
)

DEFAULT_SRC_TEST_PARAMS=( CC="${CC}" LD="${LD}" AR="${AR}" )

AT_M4DIR=( m4 )

kmod_src_install() {
    local host=$(exhost --target)
    default

    # alternatives
    dodir /usr/${host}/bin
    local a alternatives=( kmod-tools ${PN} 1000 )
    for a in depmod insmod modinfo modprobe lsmod rmmod; do
        dosym kmod /usr/${host}/bin/${a}
        alternatives+=(
            /usr/${host}/bin/${a}      ${PN}.${a}
            /usr/share/man/man8/${a}.8 ${PN}.${a}.8
        )
    done

    if ! option bash-completion ; then
        edo rm -r "${IMAGE}"/no
    fi

    alternatives+=(
        /usr/share/man/man5/depmod.d.5          ${PN}.depmod.d.5
        /usr/share/man/man5/modprobe.d.5        ${PN}.modprobe.d.5
        /usr/share/man/man5/modules.dep.5       ${PN}.modules.dep.5
        /usr/share/man/man5/modules.dep.bin.5   ${PN}.modules.dep.bin.5
    )

    alternatives_for "${alternatives[@]}"
}

