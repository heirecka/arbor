# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2008, 2009 David Leverton <dleverton@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion\
        github [ user=wofr06 tag=v${PV} ] \
        zsh-completion

SUMMARY="A preprocessor for less"
HOMEPAGE+=" https://www-zeuthen.desy.de/~friebel/unix/lesspipe.html"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# Tests are unsuccessful
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/unzip [[ note = [ Used for one test ] ]]
    build+run:
        dev-lang/perl:*
    run:
        sys-apps/less
        sys-devel/binutils [[ note = [ For ar, nm, strings; strings used without checking it exists ] ]]
        virtual/gzip
        virtual/tar
    suggestion:
        app-arch/brotli          [[ description = [ Support for brotli-compressed files and tarballs ] ]]
        app-arch/bzip2           [[ description = [ Support for bzipped files and tarballs ] ]]
        app-arch/cabextract      [[ description = [ Support for Windows cabinet archives ] ]]
        app-arch/dpkg            [[ description = [ Support for metadata of Debian packages ] ]]
        app-arch/lz4             [[ description = [ Support for lz4-compressed files and tarballs ] ]]
        app-arch/lzip            [[ description = [ Support for lzipped files and tarballs ] ]]
        app-arch/p7zip           [[ description = [ Support for the contents of 7-zip archives ] ]]
        app-arch/rpm             [[ description = [ Support for RPM packages, together with app-arch/cpio ] ]]

        ( app-arch/unrar app-arch/rar ) [[
            *description = [ Support for RAR archives ]
        ]]

        app-arch/xz              [[ description = [ Support for lzma-compressed files and tarballs ] ]]
        app-arch/zstd            [[ description = [ Support for zstd-compressed files and tarballs ] ]]

        ( app-cdr/cdrtools app-cdr/cdrkit ) [[
            *description = [ Support for information about ISO9660 filesystem images ]
        ]]

        app-crypt/gnupg          [[ description = [ Support for data encrypted by PGP or compatible programs ] ]]
        app-office/libreoffice   [[ description = [ Support for RTF, OpenDocument/OpenOffice and Microsoft Excel/Powerpoint/Word documents ] ]]

        app-text/djvu            [[ description = [ Support for DjVu documents ] ]]

        ( app-text/html2text net-www/elinks net-www/links:* net-www/lynx net-www/w3m ) [[
            *description = [ Support for HTML documents ]
        ]]

        app-text/o3read          [[ description = [ Support for OpenDocument/OpenOffice documents, together with a suitable text-mode HTML viewer ] ]]
        app-text/odt2txt         [[ description = [ Support for OpenDocument/OpenOffice documents ] ]]
        app-text/pandoc:*        [[ description = [ Support for epub, markdown, RTF, OpenDocument/OpenOffice and Microsoft Word (docx) and Powerpoint (pptx) documents ] ]]
        app-text/poppler         [[ description = [ Support for PDF documents ] ]]

        ( app-text/pstotext app-text/ghostscript ) [[
            *description = [ Support for PostScript documents ]
        ]]

        ( app-text/texlive app-text/dvi2tty ) [[
            *description = [ Support for TeX DVI documents ]
        ]]

        app-text/unrtf           [[ description = [ Support for RTF documents ] ]]
        app-text/xlhtml          [[ description = [ Support for Microsoft Excel < 2007 documents ] ]]

        ( base/antiword app-doc/catdoc app-office/libreoffice ) [[
            *description = [ Support for Microsoft Word < 2007 documents ]
        ]]

        dev-libs/openssl:*       [[ description = [ Support for crt, pem, csr, crl files ] ]]
        media/mediainfo          [[ description = [ Support for metadata of audio and video files ] ]]
        media-gfx/ImageMagick    [[ description = [ Support for metadata about images in various formats ] ]]
        media-libs/ExifTool      [[ description = [ Fallback program ] ]]

        ( media-sound/id3v2 media-sound/mp3info ) [[
            *description = [ Support for ID3 tags in MP3 files ]
        ]]

        sys-apps/groff           [[ description = [ Support for *roff documents of various kinds ] ]]
        sys-apps/musl-locales    [[ description = [ Locale binary provider for musl libc ] ]]
        sys-libs/glibc           [[ description = [ Support for some character encoding conversions on viewed files ] ]]
        virtual/cpio             [[ description = [ Support for RPM packages, together with app-arch/rpm ] ]]
        virtual/unzip            [[ description = [ Support for Zip archives and OpenDocument/OpenOffice documents ] ]]
"

src_configure() {
    # Can't use econf as it's not autotools, just a hand-written perl script
    edo ./configure --prefix="/usr/$(exhost --target)"
}

src_compile() {
    # Don't run ./configure twice
    :
}

src_test() {
    export LESSOPEN="| ./lesspipe.sh %s"

    default
}

src_install() {
    # Allow Paludis to manage bash and zsh completion files
    edo sed '/bash/d' -i "${WORK}"/Makefile
    edo sed '/zsh/d' -i "${WORK}"/Makefile

    default

    dobashcompletion "${WORK}"/less_completion
    dozshcompletion "${WORK}"/_less
    # man-pages are platform-independent
    edo mv "${IMAGE}"/usr/{$(exhost --target),}/share/man
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share

    hereenvd 70lesspipe <<EOF
LESSOPEN="| lesspipe.sh %s"
EOF
}

