# Copyright 2011-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN/fonts/base35-fonts}

require github [ user=ArtifexSoftware pn=${MY_PN} tag=${PV} ]

SUMMARY="Free versions of the 35 standard PostScript fonts"

LICENCES="AGPL-3"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES=""

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/ae671b1b1f4219b090628f259b994eada336d965.patch
    "${FILES}"/${PN}-20200910-dont-config-d050000l-as-fantasy-font.patch
)

src_install() {
    default

    insinto /usr/share/fonts/X11/${PN}
    doins fonts/*.{afm,t1,otf}

    # No use-cases for this as far as we know
    edo rm fontconfig/urw-fallback-generics.conf

    # Install the fontconfig files with correct priority for our distribution
    local fontconfig_prio=61
    for file in fontconfig/*.conf; do
        # Rename
        DISTRO_FILENAME="${fontconfig_prio}-$(basename $file)"
        edo mv $file $DISTRO_FILENAME

        # Install
        insinto /usr/share/fontconfig/conf.avail
        doins *.conf

        # Enable by default
        dodir /etc/fonts/conf.d
        edo ln -sf /usr/share/fontconfig/conf.avail/$DISTRO_FILENAME "${IMAGE}"/etc/fonts/conf.d/$DISTRO_FILENAME
    done

    insinto /usr/share/metainfo
    doins appstream/*.xml
}

